<?php

namespace Drupal\search_api_location_views_plugin\Plugin\views\filter;

use Drupal\geofield\Plugin\views\filter\GeofieldProximityFilter;

/**
 * Defines a filter for filtering on location fields.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("search_api_location_geofield_proximity")
 */
class SearchApiFilterLocationGeofieldProximity extends GeofieldProximityFilter {

  /**
   * Display the filter on the administrative summary.
   */
  public function adminSummary() {
    $output = parent::adminSummary();
    return $this->operator . ' ' . $output;
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    // Cut the geofield operators to be consistent with search api solr.
    // @see Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend::createLocationFilterQuery()
    // Also by now we can just use < operator... @see
    // Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend::setSpatial()
    // to see the restriction
    //
    // We keep it as we don't want to mess up the form
    // and its states. just disable in view form @todo add whenever is solved
    // in search_api_solr side
    $operators = parent::operators();
    unset($operators['=']);
    unset($operators['!=']);
    unset($operators['<>']);
    unset($operators['not between']);
    unset($operators['in']);
    unset($operators['not in']);
    // @todo These operators may work but not by now @see note above
    // unset($operators['between']);
    unset($operators['>']);
    unset($operators['>=']);
    unset($operators['<=']);

    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $this->request->getCurrentRequest();
    $proximity_filter_get = $request->get($this->options['expose']['identifier']);
    try {
      /** @var \Drupal\geofield\Plugin\GeofieldProximitySourceInterface $source_plugin */
      $this->sourcePlugin = $this->proximitySourceManager->createInstance($this->options['source'], $this->options['source_configuration']);
      $this->sourcePlugin->setViewHandler($this);
      $this->sourcePlugin->setUnits($this->options['units']);
      $info = $this->operators();
      // Add query condition in case of valid proximity filter options.
      if ($haversine_options = $this->getHaversineOptions()) {
        $this->{$info[$this->operator]['method']}($haversine_options);
      }
      // Otherwise output empty result in case of unexposed proximity filter.
      elseif (!$this->isExposed()) {
        // Origin is not valid so return no results (if not exposed filter).
        drupal_set_message($this->t('The location %location could not be resolved and was ignored.', ['%location' => $this->value['value']]), 'warning');
      }
    }
    catch (\Exception $e) {
      watchdog_exception('search_api_location', $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opBetween($options) {
    // @todo readd between whenever is accepted in search_api_solr
    drupal_set_message($this->t('The operator between is not accepted, it must be disabled by admin... %location could not be resolved and was ignored.', ['%location' => $this->value['value']]), 'warning');
    return;
    if (!empty($this->value['min']) && is_numeric($this->value['min']) &&
      !empty($this->value['max']) && is_numeric($this->value['max'])) {
      /** @var \Drupal\views\Plugin\views\query\Sql $query */
      $query = $this->query;

      $location_options[] = $options + [
        'radius' => $this->convertRadius($this->value['max']),
        'min_radius' => $this->convertRadius($this->value['min']),
      ];

      $query->setOption('search_api_location', $location_options);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple($options) {

    if (!empty($this->value['value']) && is_numeric($this->value['value'])) {
      /** @var \Drupal\views\Plugin\views\query\Sql $query */
      $query = $this->query;
      $location_options[] = $options + [
        'radius' => $this->convertRadius($this->value['value']),
      ];
      $query->setOption('search_api_location', $location_options);
    }
  }

  /**
   * Gets the haversine options.
   *
   * @return array
   *   The haversine options.
   *
   * @throws \Drupal\geofield\Exception\HaversineUnavailableException;
   */
  protected function getHaversineOptions() {

    // Lan and lon.
    $origin = $this->sourcePlugin->getOrigin();
    if (empty($origin['lat']) && empty($origin['lon'])) {
      return NULL;
    }
    if (!$origin || (!is_numeric($origin['lat']) && !is_numeric($origin['lon']))) {
      throw new HaversineUnavailableException('Not able to calculate Haversine Options due to invalid Proximity origin location.');
    }

    $haversine_options = [
      'field' => $this->realField,
      'lat' => $origin['lat'],
      'lon' => $origin['lon'],
    ];

    return $haversine_options;
  }

  /**
   * Get the radius in specified units.
   *
   * @param float $quantity
   *   The quantity of the configured units to be multiplied by its unit factor.
   *
   * @return string
   *   The radius in specified units.
   */
  protected function convertRadius($quantity) {
    // If the radius isn't numeric omit it. Necessary since "no radius" is "-".
    $radius = (!is_numeric($quantity)) ? NULL : $quantity;
    $units = array_column($this->getUnits(), 'multiplier', 'id');
    $multiplier = $this->getUnits()[$this->options['units']]['multiplier'];
    $radius *= $multiplier;

    return $radius;
  }

  /**
   * Returns an array of distance units.
   *
   * Distance searching is kilometer based, so all multipliers must be relative
   * to 1 kilometer.
   *
   * @return array
   *   An associative array with supported distance units and their
   *   specifications.
   */
  protected function getUnits() {
    return [
      'GEOFIELD_KILOMETERS' => [
        'id' => 'km',
        'multiplier' => 1,
        'label' => t('Kilometers'),
        'abbreviation' => 'km',
      ],
      'GEOFIELD_MILES' => [
        'id' => 'mi',
        'multiplier' => 1.60935,
        'label' => t('Miles'),
        'abbreviation' => 'mi',
      ],
      'GEOFIELD_YARDS' => [
        'id' => 'yd',
        'multiplier' => 0.0009144,
        'label' => t('Yards'),
        'abbreviation' => 'yd',
      ],
      'GEOFIELD_FEET' => [
        'id' => 'ft',
        'multiplier' => 0.0003048,
        'label' => t('Feet'),
        'abbreviation' => 'ft',
      ],
      'GEOFIELD_NAUTICAL_MILES' => [
        'id' => 'nmi',
        'multiplier' => 1.852,
        'label' => t('Nautical miles'),
        'abbreviation' => 'nmi',
      ],
      'GEOFIELD_METERS' => [
        'id' => 'm',
        'multiplier' => 0.001,
        'label' => t('Meters'),
        'abbreviation' => 'm',
      ],

    ];
  }

}
